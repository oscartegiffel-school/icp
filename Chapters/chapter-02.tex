% Chapter 2

\chapter{Describing Syntax and Semantics} 

\label{Chapter2} 
Een taal of programmeertaal bestaat uit een set karakters. Deze karakters komen van een bepaald alfabet en samengevoegd worden het zinnen of, bij een programmeertaal, statements. De \emph{syntax} regels van een taal specificeren welke reeks karakters in de taal voorkomen. Engels bijvoorbeeld heeft een grote en complexe collectie van regels om de syntax te specificeren. 

In een programmeertaal worden de statements die gemaakt zijn opgedeeld in kleinere delen genaamd \emph{lexemes}. Een programma kan beter gezien worden als een reeks van lexemes dan een reeks van characters. Lexemes worden ingedeeld in groepen. Variabele namen, methodes, classes etc. vormen samen de groep \emph{identifiers}. Elke groep lexemes word gerepresenteerd door middel van een naam oftewel een \emph{token}. Een token is dus een groep lexemes. De eerste stap van de compiler is de \emph{Lexical Analyzer}. De lexical analyzer krijgt als input statements en knipt deze in een serie van tokens. Als een token niet voldoet onjuist is dan word er een error gegeven. De termen \emph{token} en \emph{lexeme} worden nogal door elkaar gehaald. Een lexeme is een instantie van een token en een token is een symbool of sequentie van characters aanduid als identifier \autocite{compilers-principles}. In tabel \ref{tab:lexemes} en tabel \ref{tab:tokens-and-lexemes} zijn een aantal voorbeelden opgenomen van lexemes en hoe tokens zich verhouden tot lexemes.

\begin{table}
\caption{Voorbeelden van lexemes}
\label{tab:lexemes}
\centering
	\begin{tabular}{l l}
		\toprule
			\tabhead{Lexemes} & \tabhead{Tokens} \\
		\midrule
			index &	identifier \\
			= & equal\_sign \\
			2 & int\_literal \\
			* & mult\_op \\
			count & identifier \\
			+ & plus\_op \\
			17 & int\_literal \\
			; & semicolon \\
		\bottomrule\\
	\end{tabular}
\end{table}

\begin{table}
\caption{Voorbeelden van tokens en lexemes}
\label{tab:tokens-and-lexemes}
\centering
	\begin{tabular}{l l l}
		\toprule
			\tabhead{Token} & \tabhead{Beschrijving} & \tabhead{Voorbeeld lexemes} \\
		\midrule
			if & characters i, f & if \\
			else & characters e, l, s, e & else \\
			comparison & < or > or <= or >= or == or !=  & <=, != \\
			id & letter followed by letters and digits & pi, score, D2 \\
			number & any numeric constant & 3.14159, 0, 6.02e23 \\
			literal & anything but ", surrounded by "'s & "core dumped" \\
		\bottomrule\\
	\end{tabular}
\end{table}


\section{Formal Methods of Describing Syntax}
In deze paragraaf wordt verslag gedaan naar de formele taalgenereermechanismen, ofwel grammars, met betrekking tot de programmeertaal GoLang. Grammars worden gebruikt om de syntax van een bepaalde programmeertaal te beschrijven.

Een meta taal is een taal die gebruikt wordt om een andere taal te beschrijven. \'{E}\'{e}n meta taal is bijvoorbeeld \emph{Backus-Naur Form}, oftewel \emph{BNF}. Deze taal is ontwikkeld om programmeer talen te beschrijven. BNF gebruikt abstracties voor syntax structuren. Een simpele voorbeeld is het toekennen van een waarde aan een variable in bijvoorbeeld Java. Als het omgeschreven wordt naar BNF is het als volgt; \mintinline{text}{<assign> -> <var> = <expression>}. De linker kant, oftewel LHS, is geeft aan welke abstractie er wordt gedefinieerd. De rechterzijde, oftewel RHS, geeft aan met tokens, lexemes en references naar andere abstracties de definitie aan van de \emph{regel}.

De abstracties in een BNF beschrijving, of \emph{grammar}, worden vaak \emph{nonterminal symbols} of \emph{nonterminals} genoemd. De lexemes and tokens van de regels worden \emph{terminal symbols} of \emph{terminals} genoemd. Je kan zeggen dat; \keyword{een BNF beschrijving, of grammar, een collectie is van regels.}

In code voorbeeld \ref{code:variable-for-bnf} is te zien hoe in GoLang een waarde toekennen aan een variable. GoLang wordt beschreven als een dialect van BNF, de \emph{Extended Backus-Naur-Form} (EBNF). Hieronder is de specificatie van de EBNF notatie.

\begin{listing}
\begin{minted}{text}
Production  = production_name "=" [ Expression ] "." .
Expression  = Alternative { "|" Alternative } .
Alternative = Term { Term } .
Term        = production_name | token [ "…" token ] | Group | Option | Repetition .
Group       = "(" Expression ")" .
Option      = "[" Expression "]" .
Repetition  = "{" Expression "}" .
\end{minted}
\caption{EBNF}
\label{code:ebnf}
\end{listing}

Deze EBNF notatie wordt gebruikt om regels vast te stellen voor de programmeer taal. Hieronder zijn een aantal regels voor GoLang die nodig zijn om een waarde toe te kennen aan een variable.

\begin{listing}

\begin{minted}{text}
ShortVarDecl = IdentifierList ":=" ExpressionList.
VarDecl     = "var" ( VarSpec | "(" { VarSpec ";" } ")" ).
VarSpec     = IdentifierList ( Type [ "=" ExpressionList ] | "=" ExpressionList ).

ExpressionList = Expression { "," Expression } .
Expression = UnaryExpr | Expression binary_op Expression .
UnaryExpr  = PrimaryExpr | unary_op UnaryExpr .
binary_op  = "||" | "&&" | rel_op | add_op | mul_op .
rel_op     = "==" | "!=" | "<" | "<=" | ">" | ">=" .
add_op     = "+" | "-" | "|" | "^" .
mul_op     = "*" | "/" | "%" | "<<" | ">>" | "&" | "&^" .	
unary_op   = "+" | "-" | "!" | "^" | "*" | "&" | "<-" .


IdentifierList = identifier { "," identifier } .
identifier = letter { letter | unicode_digit } .	
letter        = unicode_letter | "_" .
decimal_digit = "0" … "9" .
octal_digit   = "0" … "7" .
hex_digit     = "0" … "9" | "A" … "F" | "a" … "f" .

\end{minted}
\caption{EBNF GoLang}
\label{code:ebnf}
\end{listing}

Met deze EBNF notatie is het mogelijk om code voorbeeld \ref{code:variable-for-bnf} te schrijven.

\begin{listing}[H]
	\inputminted{go}{Code/variable.go}
	\caption{Variable declaratie}
	\label{code:variable-for-bnf}
\end{listing}


In GoLang is het ook mogelijk om in een verkorte vorm waardes toe te kennen aan variabelen. In code voorbeeld \ref{code:variable-for-bnf-short} is te zien hoe er in GoLang een variable kan worden gedeclareerd in de verkorte notatie. 

\begin{listing}[H]
	\inputminted{go}{Code/variable-short.go}
	\caption{Variable declaratie short}
	\label{code:variable-for-bnf-short}
\end{listing}


\section{Attribute Grammars}
Een attribute grammar is een tool dat wordt gebruikt om meer van de structuur van een programmeer taal te beschrijven dan met \emph{context-free grammar} mogelijk is. Een attribute grammar is een toevoeging op \emph{contact-free grammar}. Met de toevoeging kunnen bepaalde taalregels gemakkelijk worden beschreven, zoals type compatibiliteit. 

\emph{Attribute grammar} is ontstaan omdat niet alle mogelijkheden van een programmeer taal uit te drukken zijn in BNF. Het is bijvoorbeeld niet mogelijk om aan te geven dat een variable gedeclareerd moet zijn voordat er naar gerefereerd is, of om aan te geven dat een string niet kan worden toegekend aan een string variable. De code in voorbeeld \ref{code:type-compatability} geeft een error dat het niet mogelijk is. 

\begin{listing}[H]
	\inputminted{go}{Code/typecompatibility.go}
	\caption{Type compatability}
	\label{code:type-compatability}
\end{listing}

Door gebruikt te maken van attribute grammar is het mogelijk om dit te modeleren. Deze problemen die niet te modeleren zijn in BNF, illustreren de categorie\"{e}n van taalregels die worden \emph{static semantics} regels genoemd. De \emph{static semantics} van een taal is alleen direct gerelateerd aan de betekenis van programma's tijdens de uitvoering; het heeft te maken met de juridische vormen van programma's (syntaxis in plaats van semantiek). \emph{Static semantics} heet zo omdat de analyse die nodig is om de specificaties te controleren, kan worden gedaan tijdens het compileren. Attribute gramar breid context-free grammar uit met de volgende functionaliteiten;

\begin{itemize}
	\item Elke grammar symbool X heeft een set van attributen A(X). The set A(X) bestaat uit twee sets \emph{synthesized attributes} S(X) en \emph{inherited attributes} I(X). \emph{Synthesized attributes} worden gebruikt om semantische informatie door te geven omhoog in de \emph{parse tree}, terwijl \emph{inherited attributes} gebruikt worden om informatie door te geven naar beneden en horizontaal in de parse tree.
	\item Elke grammar heeft een set van semantische functies en een mogelijke lege set predikaatfuncties boven de attributen van de symbolen in de grammatica regel.
	\item Een predikaatfunctie heeft de vorm van een Boolean uitdrukking op de unie van de attributenset, waarbij alle predikaatfuncties waar moeten zijn om geen overtreding van de syntaxis of statische semantiekregels van de taal te hebben.
\end{itemize}

In code voorbeeld \ref{code:attribute-grammar} is \emph{attribute grammar} te zien voor simpele statements. De attributen van de \emph{nonterminals} worden als volgt gebruikt;

\begin{description}[align=left]
	\item [actual\_type] Een \emph{synthesized attribuut} dat is gekoppeld aan de nonterminals \mintinline{text}{<var>} en \mintinline{text}{<expr>}. Het wordt gebruikt om het type op te slaan van een variabele of expressie. 
	\item [expected\_type] Een \emph{inherited attribute} dat is gekoppeld aan de \emph{nonterminal} \mintinline{text}{<expr>}. Dit wordt gebruikt om het type op te slaan dat wordt verwacht van de expressie, zoals die wordt afgeleid van het type van de variable aan LHS van de statement.
\end{description}

\begin{listing}[H]
	\begin{minted}{text}
1.  Syntax rule: <assign> -> <var> = <expr>
	Semantic rule: <expr>.expected_type <- <var>.actual_type
2.  Syntax rule: <expr> -> <var>[2] + <var>[3] 
	Semantic rule: <expr>.actual_type <-
		if (<var>[2].actual_type = int) and (<var>[3].actual_type = int)
			then int 
		else real
	end if
	Predicate: 	<expr>.actual_type == <expr>.expected_type
3.  Syntax rule: <expr> -> <var>
	Semantic rule: <expr>.actual_type <- <var>.actual_type
	Predicate: <expr>.actual_type == <expr>.expected_type 
4.  Syntax rule: <var> -> A | B | C
	Semantic rule: <var>.actual_type <- look-up(<var>.string)
	\end{minted}
	\caption{Attribute grammar}
	\label{code:attribute-grammar}
\end{listing}

De \emph{attribute grammar} van GoLang gaat dan als volgt. 

\begin{minted}{go}

// <expr>.expected_type <- <var>.actual_type

var i int = 15
\end{minted}


\section{Describing the Meanings of Programs: Dynamic Semantics}
In deze paragraaf worden \emph{dynamic semantics}, of de betekenis van de expressions, statements, en programma eenheden van een taal beschreven. Er is geen algemene geaccepteerde notatie of nadering voor \emph{dynamic semantics} maar er zijn wel verschillende methoden ontwikkeld. \emph{Dynamic semantics} worden gebruikt voor als een programmeur moet weten wat een statement in een programmeer taal doet voordat ze het kunnen implementeren in hun eigen programma of als een de maker van een compiler moet begrijpen wat een samenstelling betekent. Het helpt programmeer taal ontwerpers ook om fouten op te sporen in hun eigen programmer taal. Als er bij elke programmeertaal een complete en correcte beschrijving zou zijn van de taal, dan zouden er geen testen of correctheid nodig zijn. Desondanks wordt er niet vaak een complete syntax en semantische beschrijving gegeven en zijn testen en correctheid alsnog nodig. 

Bij een imperatieve programmeertaal zijn er drie methoden om de \emph{dynamic semantics} te beschrijven. In de volgende paragrafen woorden deze methoden uitgelegd.

\subsection{Operational semantics}
Het idee achter \emph{operational semantics} is om de betekenis van een statement of programma te beschrijven door de effecten van het uitvoeren ervan op een machine te specificeren. Er zijn verschillende niveaus van het gebruiken van \emph{operational semnatics}. Op het hoogste niveau ligt het interessegebied in het eind resultaat van het uitvoeren van het complete programma. Dit wordt soms ook wel \emph{natural operational semantics} genoemd. Op het laagste niveau kan de \emph{operational semantics} worden gebruikt om de precieze betekenis van een programma te bepalen door middel van een onderzoek van de volledige reeks state wijzigingen die optreden wanneer het programma wordt uitgevoerd. Dit wordt ook wel \emph{structural operational semantics} genoemd. 


\subsection{Denotational semantics}
\emph{Denotational semantics} is de meest grondige en bekende formele methode om de betekenis van een programma te beschrijven. Het is gebaseerd op de \emph{recursive function theorie}. Het proces van het construeren van een \emph{Denotational semantics specificatie} voor een programmeertaal, vereist dat voor elke taalentiteit zowel een wiskundig object als een functie wordt gedefinieerd, die instanties van die taalentiteit op instanties van het wiskundige object afbeeldt. Omdat de objecten rigoureus zijn gedefinieerd, modelleren ze de exacte betekenis van hun bijbehorende entiteiten. Het idee is gebaseerd op het feit dat er rigoureuze manieren zijn om wiskundige objecten te manipuleren, maar geen programmeertaalconstructies. De moeilijkheid met deze methode ligt in het maken van de objecten en de mapping-functies. De methode wordt \emph{denotational} genoemd omdat de wiskundige objecten de betekenis van hun syntactische entiteiten aangeven.
De mappingfuncties van een \emph{denotational semantics} programmeertaal specificatie hebben een domein en een bereik. Het domein is de verzameling waarden die legitieme parameters zijn voor de functie; het bereik is de verzameling van objecten waarnaar de para- meters worden afgebeeld. In denotationele semantiek wordt het domein het syntactische domein genoemd, omdat het syntactische structuren zijn die in kaart worden gebracht. Het bereik wordt het semantische domein genoemd.
\emph{Denotational semantics} is gerelateerd aan operationele semantiek. In operationele semantiek worden programmeertaalconstructies vertaald in eenvoudigere programmeertaalconstructies, die de basis van de betekenis van het construct worden. In \emph{denotational semantics} worden programmeertaalconstructies toegewezen aan wiskundige objecten, ofwel sets of, vaker, functies. In tegenstelling tot operationele semantiek modelleert \emph{denotational semantics} echter niet de stapsgewijze berekening van programma's.

\subsection{Axiomatic semantics}
\emph{Axiomatic semantics} is de meest abstracte benadering van de specificatie van de semantiek. In plaats van rechtstreeks de betekenis van een programma te specificeren, specificeert \emph{axiomatic semantics} wat van het programma kan worden bewezen. Bedenk dat een van de mogelijke toepassingen van semantische specificaties is om de juistheid van programma's aan te tonen. In de \emph{axiomatic semantics} is er geen model van de toestand van een machine of programma of model van toestandsveranderingen die plaatsvinden wanneer het programma wordt uitgevoerd. De betekenis van een programma is gebaseerd op relaties tussen programmavariabelen en constanten, die hetzelfde zijn voor elke uitvoering van het programma. \emph{Axiomatic semantics} heeft twee verschillende toepassingen: programmaverificatie en specificatie van de programma-semantiek. Deze sectie concentreert zich op programmaverificatie in zijn beschrijving van \emph{axiomatic semantics}. \emph{Axiomatic semantics} werd gedefinieerd in samenhang met de ontwikkeling van een aanpak om de juistheid van programma's te bewijzen. Dergelijke correctheidsbewijzen tonen aan dat wanneer een programma kan worden geconstrueerd, een programma de berekening uitvoert die wordt beschreven in de specificatie. In een proef wordt elke instructie van een programma zowel voorafgegaan als gevolgd door een logische uitdrukking die beperkingen specificeert op programmavariabelen. Deze kan worden gebruikt om de betekenis van de verklaring te specificeren. De notatie die wordt gebruikt om constraints te beschrijven is predikaat calculus. Hoewel eenvoudige \emph{boolean} expressies vaak voldoende zijn om beperkingen uit te drukken, is dat soms niet het geval. Wanneer \emph{axiomatic semantics} wordt gebruikt om formeel de betekenis van een verklaring te specificeren, wordt de betekenis gedefinieerd door het effect van de verklaring op beweringen over de gegevens die door de verklaring worden be\"{i}nvloed.





