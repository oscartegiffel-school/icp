\chapter{Names Bindings, and Scopes}
Dit hoofdstuk introduceert de fundamentele semantische kwesties die bij variabelen naar voren komen in Go. Het begint bij het beschrijven van de aard van namen en speciale woorden. Vervolgens worden de attributen, types, adressen en waarden besproken. En tot slot worden belangrijke concepten van bindingen, binding tijden en scope regels beschreven.

\section{Names}
Namen in een programmeertaal hebben twee primaire ontwerp problemen:
\begin{itemize}
	\item  Zijn de namen hoofdlettergevoelig?
	\item  Zijn de speciale woorden van de taal gereserveerde woorden of trefwoorden?
\end{itemize}

\subsection{Name forms}
Een \emph{name} is een reeks van characters die gebruikt wordt om een bepaald entiteit van een programma te identificeren. Namen hebben in de meeste programmeer talen dezelfde vorm; een letter gevolgd door een string bestaande uit letters, cijfers en/of underscore. Bij Golang is er geen limiet van de variable naam en zijn namen hoofdlettergevoelig. Er zijn verschillende conventies om namen te defini\"{e}ren, waaronder \emph{camelCase}, \emph{PascalCase} en \emph{snake\_case}. Bij de meeste programmeer talen is er een conventie om variabele namen als \emph{camelcase} te noteren. Een voorbeeld van camelcase;

\begin{minted}{go}
	 var firstName = "first" // met camelcase 
	 var firstname = "first" // zonder camelcase
\end{minted}

\subsection{Special Words}
\emph{Special words} zijn woorden die niet gebruikt mogen worden als namen omdat ze gereserveerd zijn door de programmeertaal zelf. In GoLang zijn de volgende namen gereserveerd.

\begin{minted}{text}
break        default      func         interface    select
case         defer        go           map          struct
chan         else         goto         package      switch
const        fallthrough  if           range        type
continue     for          import       return       var	
\end{minted}

In GoLang worden deze woorden \emph{keywords} genoemd. Als een van deze woorden gebruikt word wordt er een fout gegeven door de compiler. Bijvoorbeeld;

\begin{minted}{go}
func main() {
	var func := "Oscar"
}	
// ./icp.go:5:6: syntax error: unexpected func, expecting name	
\end{minted}

\section{Binding}
Een \emph{binding} is een koppeling tussen een attribuut en een entiteit, zoals een variabele en zijn type of waarde, of tussen een operatie en een symbool. Het tijdstip waarop een binding plaatsvindt, wordt \emph{binding time} genoemd. \emph{Binding} en \emph{binding time} zijn concepten in de semantiek van programmeertalen. Bindings kunnen plaatsvinden bij de \emph{design time}, \emph{language implementation time}, \emph{compile time}, \emph{load time}, \emph{link time} of \emph{run time}. Bijvoorbeeld de \emph{+} operator is gebonden aan de \emph{ADD} operatie bij de \emph{language design time}. Bijvoorbeeld het volgende statement \mintinline{go}{count := count +5}. In dit statement worden er verschillende bindings en verschillende binding times toegepast, waaronder de volgende;

\begin{itemize}
	\item Het type \emph{count} is gebonden bij de \emph{compile time}.
	\item De reeks van mogelijke waarden van \emph{count} wordt gebonden bij \emph{compiler design time}.
	\item De betekenis van het symbool \emph{+} is gebonden aan de \emph{compile time}, wanneer de soorten operatoren zijn vastgesteld.
	\item De interne representatie van de \emph{5} is gebonden aan de \emph{design time} van de compiler.
	\item De waarde van \emph{count} is op \emph{execution time} aan deze verklaring gebonden.
\end{itemize}

\subsection{Binding of Attributes to Variables}
Een \emph{binding} is \emph{static} als deze eerst optreedt voordat de looptijd begint en blijft gedurende de gehele uitvoering van het programma onveranderd. Een binding is dynamisch als de binding voor het eerst tijdens de looptijd optreedt of in de loop van de programma-uitvoering kan veranderen. 

\subsection{Type bindings}
Voordat een variabele in een programma kan worden verwezen, moet het aan een data type worden gebonden. De twee belangrijke aspecten van deze binding zijn hoe het type wordt gespecificeerd en wanneer de binding plaatsvindt. Typen kunnen statisch worden gespecificeerd door middel van een vorm van expliciete of impliciete aangifte.

Een \emph{explicit declaration} is een statement waarbij een variable naam wordt gespecificeerd met het bijhorende type. Een \emph{implicit declaration} betekent dat het type van de variable wordt bepaald door de waarde ervan. Daarnaast heb je nog \emph{type inference}. Dat betekent dat het type wordt afgeleid van een andere waarde. In GoLang ziet dat er zo uit; 

\begin{listing}[H]
	\begin{minted}{go}
var name string = "Oscar" // Explicit declaration
name := "Oscar" // Implicit declaration

var i int
j := i // j is an int, type inference
	\end{minted}
	\caption{Type declaration}
	\label{code:type-declaration}
\end{listing}

\section{Scope}
Een van de belangrijke factoren bij het begrijpen van variabelen is \emph{scope}. De \emph{scope} van een variabele is het bereik van statements waarin de variabele zichtbaar is. Een variabele is \emph{zichtbaar} in een statement als er naar kan worden verwezen of worden toegewezen. De \emph{scope rules} van een taal bepalen hoe een bepaald voorkomen van een naam geassocieerd wordt met een variabele. Met name bepalen de \emph{scope regels} hoe verwijzingen naar variabelen die buiten het  blok vallen dat momenteel wordt uitgevoerd, worden gekoppeld aan hun aangiften en dus aan hun kenmerken. Een variabele is \emph{local} in een programma of blok daar wordt gedeclareerd. De \emph{nonlocal} variabelen van een programma of blok zijn de variabelen die in het programma of blok zichtbaar zijn, maar daar niet worden aangegeven. \emph{Global variabels} zijn een speciale categorie \emph{nonlocal variables}. GoLang is een blok gestructureerde taal, dit betekent dat de scope van de variable wordt bepaald door het blok, oftewel het gedeelte tussen de accolades. Bijvoorbeeld;

\begin{listing}[H]
	\begin{minted}{go}
func main() {	
	{
		v := 1
		{
			fmt.Println(v)
		}
		fmt.Println(v)
	}
	// “undefined: v” compilation error
	//fmt.Println(v)
}
	\end{minted}
	\caption{Scope. Bron \emph{\autocite{go-scope}}}
	\label{code:scope}
\end{listing}

De variable is gedeclareerd binnen de methode en is overal binnen de methode beschikbaar. Buiten de methode is de variabele niet beschikbaar en komt er een compileer fout.

\section{Lifetime}
De \emph{lifetime} van een variabele lijkt een nauw verband te hebben met de \emph{scope} ervan. Als een variabele gedeclareerd word in een methode dan is de scope van het begin van de declaratie tot het einde van de mehode. De lifetime is dan van het begin van de uitvoering van de methode tot dat de methode klaar is met uitvoeren. Desondanks, zijn de scope en de lifetime niet hetzelfde. Bijvoorbeeld een statische variable kan een local scope hebben en alleen beschikbaar zijn binnen die methode, maar de lifetime is static en de variable bestaat voor zolang het programma uitgevoerd wordt.

\section{Referencing environments}
De \emph{referencing environments} van een statement is de verzameling van alle variabelen die zichtbaar zijn in het statement. De \emph{referencing environments} van een statement in een static-scoped taal is de variabelen die worden gedeclareerd in de lokale scope plus de verzameling van alle variabelen van de voorouder scopes die zichtbaar zijn. In het volgende voorbeeld zijn er drie \emph{reference environments} aanwezig. 

\begin{listing}[H]
	\begin{minted}{go}
const number = 5

func main() {
	{
		v := 1
		{
			p := 15
			fmt.Println(number, v, p) // Output: 5 1 15 - first environment
		}
		fmt.Println(number, v) // Output: 5 1 - second environment
	}

	fmt.Println(number) // Output: 5 - third environment
}
	\end{minted}
	\caption{Referencing environments}
	\label{code:referencing-environments}
\end{listing}

In de eerste omgeving zijn de variabelen \emph{number}, \emph{v} en.\emph{p} beschikbaar. De tweede omgeving zijn \emph{number} en \emph{v} beschikbaar en de derde omgeving is alleen \emph{number} beschikbaar.

\section{Named constants}
Een \emph{named constant} is een variabele die slechts \'{e}\'{e}n keer aan een waarde gebonden is. \emph{Constants} zijn nuttig als hulpmiddel voor de leesbaarheid en de betrouwbaarheid van het programma. De leesbaarheid kan bijvoorbeeld worden verbeterd door het gebruik van de naam pi in plaats van de constante 3.14159265. Bijvoorbeeld;

\begin{listing}[H]
	\begin{minted}{go}
const pi = 3.141592

func main() {
	fmt.Println(pi) // Output: 3.141592
}
	\end{minted}
	\caption{Constants}
	\label{code:constants}
\end{listing}


