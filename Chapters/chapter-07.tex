\chapter{Statement-Level Control Structures}
Hoewel berekeningen kunnen worden uitgevoerd door evaluatie van statements is dit alleen niet voldoende voor een programmeertaal. Wat ook nodig is, zijn \emph{control statements}. Dit zijn manieren om de \emph{control flow paths} te selecteren en manieren om statements te herhalen. Op deze manier kan de control flow van een programma worden bestuurd door de programmeur. GoLang heeft verschillende vormen van control statement zodat de flow van het programma in de handen ligt van de programmeur.

\section{Selection Statements}
Een \emph{selection statement} geeft de mogelijkheid om te kiezen tussen twee of meer executie paden in een programma. Deze statements zijn fundamenteel en een essentieel onderdeel van alle programmeer talen. Selection statements vallen in twee algemene categorie\'{e}n; \emph{two-way} en \emph{n-way} of \emph{multiple selection}. 

\subsection{Two-way selection statements}
Een vorm van selection statements zijn de \emph{two-way selection statements}. Zoals de naam al aangeeft is dit een manier om twee verschillende paden van executie flows te cre\"{e}ren. Het algemene idee van een two-way statement wordt gemaakt met de keywords \emph{if}, \emph{then} en \emph{else}. Bij GoLang is er niet een keyword \emph{then}, dit onderdeel van de two-way selection statement wordt aangegeven met accolades. In verschillende code voorbeelden is al gebruik gemaakt van een if-statement maar hieronder is nogmaals een voorbeeld te zien voor verduidelijking;

\begin{listing}[H]
	\begin{minted}{go}
package main

import "fmt"

func main() {
    if 7%2 == 0 {
        fmt.Println("7 is even")
    } else {
        fmt.Println("7 is odd")
    }
    		
	if num := 10; num % 2 == 0 { 
        fmt.Println(num,"is even") 
    } else {
        fmt.Println(num,"is odd")
    }
}		
	\end{minted}
	\caption{Two-way selection}
	\label{code:two-way-selection}	
\end{listing}

Hierbij is zijn verschillende vormen van two-way selection statements te zien. Bij GoLang is er de mogelijkheid om optioneel een statement toe te voegen die wordt uitgevoerd voordat de conditie wordt ge\"{e}volueerd. Dit wordt veelal gebruikt om variabelen te initialiseren zoals te zien is in het tweede if-statement. Hier wordt er een variable gemaakt \emph{num} met de waarde 10. De output van het code voorbeeld \ref{code:two-way-selection} is;

\begin{minted}{text}
	7 is odd
	10 is even
\end{minted}

Het eerste if-statement kan je lezen als volgt; \emph{als (\mintinline{go}{if})} de waarde van \mintinline{go}{7 % 2} gelijk is aan \mintinline{go}{0}, \emph{dan (\mintinline{go}{than})} print \mintinline{go}{7 is even}, \emph{anders (\mintinline{go}{else})} print \mintinline{go}{7 is odd}.

Het is mogelijk om meerdere if-statements te \emph{nesten}. Hiermee wordt er aangegeven dat er verschillende niveau's van if-statements worden gebruikt. Hierdoor kan de flow van het programma worden afgetakt binnen aftakkingen. Hieronder is een voorbeeld te zien het toepassen van geneste if-statements.

\begin{listing}[H]
	\begin{minted}{go}
package main

import "fmt"

func main() {
	/* local variable definition */
	var a int = 100
	var b int = 200
	
	/* check the boolean condition */
	if( a == 100 ) {
		/* if condition is true then check the following */

		if( b == 200 )  {
			/* if condition is true then print the following */
			fmt.Printf("Value of a is 100 and b is 200\n" );
		}
	}
	fmt.Printf("Exact value of a is : %d\n", a );
	fmt.Printf("Exact value of b is : %d\n", b );
}

	// Value of a is 100 and b is 200
// Exact value of a is : 100
// Exact value of b is : 200
	\end{minted}
	\caption{Nested if-statement}
	\label{code:nested-if-statement}
\end{listing} 

\subsection{Multiple-selection statements}
Een andere vorm van \emph{control statements} zijn \emph{multiple-selection statements} of \emph{n-way statements}. Deze geven de mogelijkheid om een willekeurig aantal statements te selecteren. 

In GoLang zijn er verschillende mogelijkheden om multiple-selection toe te passen. Dit kan door verschillende \emph{if-else statements} aan elkaar te koppelen of door gebruik te maken van de \emph{switch} statement. In GoLang hoeven de switch statements geen constanten of zelfs getallen te zijn. De cases worden van boven naar beneden ge\"{e}valueerd totdat een overeenkomst wordt gevonden. Als er geen overeenkomst wordt gevonden dan wordt er true gebruikt om iets te voeren. Het is daarom gelijkwaardig aan een if-else statement alleen de syntax is anders. 

Anders dan bij andere programmeertalen wordt bij GoLang alleen de geselecteerde case uitgevoerd en niet de andere cases. Daarom is het niet nodig om keywords zoals \emph{break} te gebruiken om iedere case te eindigen. Dit doet GoLang automatisch voor je. Ook anders dan bij andere programmeertalen hoeft een switch statement niet een conditie te defini\"{e}ren. Dit kan gebruikt worden om op een nette manier om lange if-then-else statements te schrijven.

\begin{listing}[h]
		\begin{minted}{go}
package main

import "fmt"

func main() {	
    i := 2
    fmt.Print("Write ", i, " as ")
    switch i {
	    case 1:
	        fmt.Println("one")
	    case 2:
	        fmt.Println("two")
	    case 3:
	        fmt.Println("three")
	    default: 
	        fmt.Println("")
    }

    fmt.Print("Write ", i, " as ")
    if i == 1 {
        fmt.Println("one")
    } else if i == 2 {
        fmt.Println("two")
    } else if i == 3 {
        fmt.Println("three")
    } else {
        fmt.Println("")
    }
}

		\end{minted}
		\caption{Multiple-selection statement}
		\label{code:multiple-selection-statement}
\end{listing}

Bij de switch statement is het mogelijk om een \emph{default} waarde mee te geven als geen van de gegeven cases overeenkomt. Dit is vergelijkbaar met het \emph{else} gedeelte in een \emph{if-else} statement. Bij GoLang is het ook mogelijk om verschillende switch statements te combineren door ze te scheiden met een komma.

\begin{listing}[H]
	\begin{minted}{go}
package main
import "fmt"

func main() {
	switch dayOfWeek := 5; dayOfWeek {
		case 1, 2, 3, 4, 5:
			fmt.Println("Weekday")
		case 6, 7:
			fmt.Println("Weekend")
		default:
			fmt.Println("Invalid Day")		
	}
}

	\end{minted}
	\caption{Combining multiple switch cases}
	\label{code:combining-multiple-switch-cases}
\end{listing}

Een van de unieke eigenschappen van GoLang is het gebruik van \emph{channels}. Een channel is een type conduit waar je waardes naar toe kan verzenden of ontvangen. In GoLang kan er worden "gewacht" tot de gekozen channels waarden ontvangen om iets uit te voeren met het \emph{select} statement. Dit is vergelijkbaar met een switch statement maar een switch statement wordt eenmalig uitgevoerd terwijl een select statement wacht op een waarde. Je krijgt bij een select statement meerdere valide waardes terug op een willekeurig moment. Deze vorm van Hieronder een voorbeeld hoe een select statement kan worden toegepast.

\begin{listing}[H]
	\begin{minted}{go}
package main

import "time"
import "fmt"

func main() {


    c1 := make(chan string)
    c2 := make(chan string)

    go func() {
        time.Sleep(1 * time.Second)
        c1 <- "one"
    }()
    go func() {
        time.Sleep(2 * time.Second)
        c2 <- "two"
    }()


    for i := 0; i < 2; i++ {
        select {
        case msg1 := <-c1:
            fmt.Println("received", msg1)
        case msg2 := <-c2:
            fmt.Println("received", msg2)
        }
    }
}

// Output:
// received one
// received two

	\end{minted}
	\caption{Select statement}
	\label{code:select-statement}
\end{listing}


\section{Iterative Statements}
Een \emph{iterative statement} zorgt ervoor dat een statement of een gedeelte van de applicatie meerdere \'{e}\'{e}n of meerdere malen wordt uitgevoerd. Dit wordt vaak een \emph{loop} genoemd. Dit is een fundamentele eigenschap dat een programmeur de mogelijkheid geeft om repetitie te voorkomen. De eerste vorm die beschikbaar is in GoLang is \emph{for-loop};

\begin{listing}[H]
	\begin{minted}{go}
package main

import "fmt"

func main() {
	sum := 0
	for i := 0; i < 10; i++ {
		sum += i
	}
	fmt.Println(sum)
}

	\end{minted}
	\caption{For loop}
	\label{code:for-loop}
\end{listing}

Deze code wordt onmiddellijk uitgevoerd voor een van te voren gespecificeerde aantal cyclussen. Een for-loop bestaat uit drie componenten die gescheiden worden door een puntkomma.  

\begin{itemize}
	\item de init statement; wordt uitgevoerd voor de eerste iteratie.
	\item de condition statement; ge\"{e}volueerd voor elke iteratie.
	\item de post statement; uitgevoerd aan het einde van elke iteratie.
\end{itemize}

In code voorbeeld \ref{code:for-loop} wordt een variable \emph{"i"} gespecificeerd met de waarde \mintinline{go}{0}. Deze variabel wordt opgehoogd door \mintinline{go}{i++} tot dat het statement \mintinline{go}{i < 10} niet meer voldoet. Deze for-loop wordt in dit geval dus 10 keer uitgevoerd.

In andere programmeertalen is er een \emph{while} loop. Het aantal iteraties in een while loop wordt niet bepaald door een nummer maar door een boolean of logische expressie. In GoLang bestaat deze vorm niet en wordt een for-loop gebruikt om dit te kunnen bewerkstelligen. Door bij een for-loop de init statement en de post statement weg te laten wordt er een while loop gecre\"{e}erd. Hieronder is een voorbeeld te zien van hoe dat eruit ziet;

\begin{listing}[H]
	\begin{minted}{go}
package main

import "fmt"

func main() {
	sum := 1
	for sum < 1000 {
		sum += sum
	}
	fmt.Println(sum)
}
	\end{minted}
	\caption{While loop}
	\label{code:while-loop}
\end{listing}

In sommige programmeertalen zoals PHP en Java is er een \emph{for-each loop}. Deze loop is een eenvoudige manier om over \emph{iterable} objecten te itereren. In GoLang kan hiervoor ook de for-loop gebruikt worden in combinatie met de \emph{range} functie. Deze functie kan itereren over de datastructuren \emph{array}, \emph{slice}, \emph{channels} en \emph{maps}. Hieronder is een voorbeeld te vinden van het toepassen van de range functie.

\begin{listing}[H]
	\begin{minted}{go}
package main

import "fmt"

func main() {

    nums := []int{2, 3, 4}
    sum := 0
    for _, num := range nums {
        sum += num
    }
    fmt.Println("sum:", sum)

    for i, num := range nums {
        if num == 3 {
            fmt.Println("index:", i)
        }
    }

    kvs := map[string]string{"a": "apple", "b": "banana"}
    for k, v := range kvs {
        fmt.Printf("%s -> %s\n", k, v)
    }

    for k := range kvs {
        fmt.Println("key:", k)
    }

	// Output:
	// sum: 9
	// index: 1
	// a -> apple
	// b -> banana
	// key: a
	// key: b
}
	\end{minted}
	\caption{Iteration on data structures}
	\label{code:iteration-on-data-structures}
\end{listing} 

\subsection{User-Located Loop Control Mechanisms}
In sommige situaties is het handig voor een programmeur om een locatie te kiezen voor \emph{loop control}. GoLang heeft verschillende mogelijkheden om de flow van het programma te be\"{i}nvloeden. 


\paragraph*{break} Bij een for-loop is het soms nodig om handmatig uit de loop te breken. Met \emph{break} is het mogelijk om direct de uitvoering van de control flow te stoppen en de executie van het programma voor te zetten. Een voorbeeld in GoLang;

\begin{listing}[H]
	\begin{minted}{go}
package main

import "fmt"

func main() {
    for {
        fmt.Println("loop")
        break
    }
}

// Output
// loop
	\end{minted}
	\caption{Break}
	\label{code:break}
\end{listing}

Door gebruik te maken van een for-loop zonder parameters wordt de loop oneindig uitgevoerd maar door \emph{break} te gebruiken wordt de loop be\"{e}indigd. Anders dan bij de meeste programmeertalen is het mogelijk om een break te doen naar een specifiek stuk in de code. Dit kan worden gedaan met een \emph{label}. In code voorbeeld \ref{code:break-with-label} is te zien hoe dit kan worden toegepast.

\begin{listing}[H]
	\begin{minted}{go}
package main

import "fmt"

func main() {
    Loop: for {
        fmt.Println("loop")
        break Loop
    }
}
// Output
// loop
	\end{minted}
	\caption{Break with label}
	\label{code:break-with-label}
\end{listing}

\paragraph*{continue} In sommige gevallen is het nodig om terug te gaan naar het begin van de control flow en een gedeelte ervan over te slaan. Met \emph{continue} wordt dit mogelijk gemaakt. Ook hier kan gebruik worden gemaakt van labels net zoals in code voorbeeld \ref{code:break-with-label}. Hieronder is een voorbeeld hoe \emph{continue} gebruikt kan worden in een for-loop;

\begin{listing}[H]
	\begin{minted}{go}
package main

import "fmt"

func main() {
    
    for i := 0; i < 7 ; i++ { 
        if i % 2 == 0 { 
            continue 
        }
        fmt.Println("Odd:", i)      
    }
}

// Output:
// Odd: 1
// Odd: 3
// Odd: 5

	\end{minted}
	\caption{Continue}
	\label{code:continue}
\end{listing} 

In code voorbeeld \ref{code:continue} is te zien dat er een loop wordt gemaakt die 7 iteraties heeft. Als de count van de iteratie een even getal is dan wordt \emph{continue} aangeroepen. De iteratie wordt hiermee doorbroken en de volgende iteratie wordt uitgevoerd. 

\paragraph*{fallthrough} Bij GoLang wordt bij een switch statement alleen de case uitgevoerd die overeenkomst met de statement. In sommige gevallen wil je wel een casus van een switch statement uitvoeren. Door \emph{fallthrough} is dit mogelijk;

\begin{listing}[H]
	\begin{minted}{go}
v := 42
switch v {
case 100:
	fmt.Println(100)
	fallthrough
case 42:
	fmt.Println(42)
	fallthrough
case 1:
	fmt.Println(1)
	fallthrough
default:
	fmt.Println("default")
}

// Output:
// 42
// 1
// default

	\end{minted}
	\caption{fallthrough}
	\label{code:fallthrough}
\end{listing}


\section{Unconditional Branching statement}
Een \emph{unconditional branching statement} verplaatst de controle van executie naar een bepaald deel van het programma. Een manier om dit te doen is de nogal controversi\"{e}le \emph{goto-statement} die wordt gebruikt voor onvoorwaardelijke vertakking. Deze statement kan worden gebruikt om de stroom van een programma om te leiden zonder dat een bepaalde voorwaarde nodig is. Er zijn geen beperkingen aan het gebruik ervan, waardoor het een gevaarlijke maar krachtige structuur is. De controversie is omdat de flow van het programma onduidelijk wordt voor de programmeur. Dijkstra die heeft in 1968 een paper geschreven genaamd \emph{Go To Statement Considered Harmful}. Ondanks dat het nogal een controversieel punt is in een programmeertaal, is het toch toegepast in GoLang. Hieronder is een voorbeeld te vinden van een toepassing ervan; 

\begin{listing}[H]
	\begin{minted}{go}
package main

import "fmt"

func main() {
   var a int = 10

   LOOP: for a < 20 {
      if a == 15 {
         a = a + 1
         goto LOOP
      }
      fmt.Printf("value of a: %d\n", a)
      a++     
   }  
}

// Output
// value of a: 10
// value of a: 11
// value of a: 12
// value of a: 13
// value of a: 14
// value of a: 16
// value of a: 17
// value of a: 18
// value of a: 19
	\end{minted}
	\caption{Goto}
	\label{code:goto}
\end{listing}

Als \mintinline{go}{a == 15} dan \emph{goto} gebruikt om opnieuw terug te gaan naar de loop. In code voorbeeld \ref{code:goto} is het ook mogelijk om hetzelfde te bereiken met \emph{continue}. In sommige gevallen is het wel handig om \emph{goto} te gebruiken. Zoals te zien is in code voorbeeld \ref{code:goto-useful}. Dit stuk code wordt gebruikt in de bron code van GoLang. Hier wordt Goto gebruikt om te voorkomen dat er een boolean variable moet worden gedeclareerd, alleen om de control-flow. In dit geval maakt \emph{goto} de code duidelijker en overzichtelijker. Ook is het bij GoLang niet mogelijk om goto te gebruiken om over variabelen te springen die worden gedeclareerd en kan er niet naar andere code blokken worden gesprongen.

\begin{listing}[H]
	\begin{minted}{go}
for x < 0 {
	if x > -1e-09 {
	  goto small
	}
	z = z / x
	x = x + 1
}
for x < 2 {
    if x < 1e-09 {
      goto small
    }
    z = z / x
    x = x + 1
}

if x == 2 {
	return z
}

x = x - 2
p = (((((x*_gamP[0]+_gamP[1])*x+_gamP[2])*x+_gamP[3])*x+_gamP[4])*x+_gamP[5])*x + _gamP[6]
q = ((((((x*_gamQ[0]+_gamQ[1])*x+_gamQ[2])*x+_gamQ[3])*x+_gamQ[4])*x+_gamQ[5])*x+_gamQ[6])*x + _gamQ[7]
return z * p / q

small:
  if x == 0 {
    return Inf(1)
  }
  return z / ((1 + Euler*x) * x)
}
	\end{minted}
	\caption{Goto useful}
	\label{code:goto-useful}
\end{listing}

Naast goto heeft GoLang ook nog een andere mogelijkheden tot unconditional branching statements zoals \emph{defer}. Met defer is het mogelijk om de uitvoer van een functie uit te stellen naar het einde van de executie van de huidige functie. Een voorbeeld is als volgt;

\begin{listing}[H]
	\begin{minted}{go}
package main

import "fmt"

func main() {
	defer fmt.Println("world")

	fmt.Println("hello")
}

// Output
// hello
// world

	\end{minted}
	\caption{Defer}
	\label{code:defer}
\end{listing}

De defer functie wordt direct ge\"{e}valueerd maar de executie van \mintinline{go}{fmt.Println("world")} wordt uitgesteld tot het einde van de main functie. 

\section{Guarded Commands}
Nieuwe en vrij verschillende vormen van selectie en loop structuren werden gesuggereerd door Dijkstra. Zijn primaire motivatie was het geven van controle statements die een methodologie voor het ontwerpen van een programma zouden ondersteunen die de \emph{correctheid} tijdens de ontwikkeling verzekerde in plaats van het verifi\"{e}ren of testen van voltooide programma's. De voorgestelde vorm van een \emph{guarded command} zag er als volgt uit;

\begin{listing}[H]
	\begin{minted}{bash}
if <Boolean expression> -> <statement>
[] <Boolean expression> -> <statement>
[] ...
[] <Boolean expression> -> <statement>
fi

	\end{minted}
	\caption{Guarded command proposal}
	\label{code:guarded-command-proposal}
\end{listing}

Een guarded command controleert een bepaalde conditie voordat het een statement mag uitvoeren. Het is in feite een propositie, welke true moet zijn om het betreffende statement uit te voeren. In GoLang zijn guards niet mogelijk, maar in Haskell zien guards er zo uit;

\begin{listing}[H]
	\begin{minted}{haskell}
bmiTell :: (RealFloat a) => a -> String  
bmiTell bmi  
    | bmi <= 18.5 = "You're underweight, you emo, you!"  
    | bmi <= 25.0 = "You're supposedly normal. Pffft, I bet you're ugly!"  
    | bmi <= 30.0 = "You're fat! Lose some weight, fatty!"  
    | otherwise   = "You're a whale, congratulations!"  
	\end{minted}
	\caption{Guards}
	\label{code:guards}
\end{listing}



