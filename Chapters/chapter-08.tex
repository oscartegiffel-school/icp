\chapter{Subprograms \& Implementing Subprograms}
\emph{Subprograms} zijn de fundamentele bouwstenen van programma's en behoren daarom tot de belangrijkste concepten in het ontwerpen van een programmeertaal. In dit hoofdstuk wordt er gekeken naar de het ontwerp van \emph{subprograms}, \emph{overloaded subprograms}, \emph{generic subprograms} en \emph{aliasing} en het \emph{side effect} probleem van subprograms. 

\section{Fundamentals of Subprograms}
Twee fundamentele abstractie faciliteiten kunnen worden opgenomen in een programmeertaal; \emph{process abstraction} en \emph{data abstraction}. In moderne programmeertalen wordt een collectie van statements geschreven als een subprogram. 

Een subprogramma moet altijd de volgende eigenschappen hebben;

\begin{itemize}
	\item Elke subprogramma heeft \'{e}\'{e}n enkele ingangspunt.
	\item Het oproepprogramma wordt gedurende de uitvoering van het opgevraagde deelprogramma niet uitgevoerd, wat betekent dat er maar \'{e}\'{e}n subprogramma op enig moment in uitvoering is.
	\item De \emph{control} keert altijd terug naar de beller wanneer de uitvoering van het subprogramma wordt be\"{e}indigd.
\end{itemize}

In het code voorbeeld hieronder is een subprogram(\emph{function}) te zien;

\begin{listing}[H]
\begin{minted}{go}
package main

import "fmt"

func add(x int, y int) int {
	return x + y
}

func main() {
	fmt.Println(add(42, 13))
}

// Output: 55

\end{minted}
\label{code:subprogram}
\caption{Subprogram}	
\end{listing}

De subprogam begint op regel 5, dit wordt ook wel de \emph{header} genoemd. Een header in GoLang bestaat uit het keyword \emph{func}, de naam van de subprogram, (eventuele) parameters en (eventuele) return types. In het voorbeeld is te zien dat de subprogram twee parameters accepteert met als type \emph{int} en \"{e}\'{e}n \emph{int} waarde retourneert. Anders dan bij andere programmeertalen is het mogelijk om in GoLang ook meerdere waardes te retourneren. In code voorbeeld \ref{code:multiple-return} is te zien hoe je in GoLang meerdere waardes kan retourneren. Hierbij wordt er in de header van de subprogram ook aangeven dat er twee int waardes worden geretourneerd. 

\begin{listing}[H]
	\begin{minted}{go}
package main

import "fmt"

func vals() (int, int) {
    return 3, 7
}

func main() {
    a, b := vals()
    fmt.Println(a)
    fmt.Println(b)
}

	\end{minted}
	\caption{Multiple return}
	\label{code:multiple-return}
\end{listing}

De header wordt gevolgd door de \emph{formal parameters} van de subprogram. Deze houden geen waarde totdat het programma wordt aangeroepen, \emph{subprogram call}. Het aanroepen van de subprogram zorgt ervoor dat de parameters de waardes vast houden, de \emph{actual parameters}. Bij de meeste programmeertalen wordt de relatie tussen de actual parameters en formal parameters bepaald door de positie in de header. Deze vorm van \emph{positional parameters} wordt ook gebruikt bij GoLang. Een andere vorm die gebruikt kan worden is \emph{keyword parameters}. Deze vorm komt voor bij bijvoorbeeld Python of R. In code voorbeeld \ref{code:keyword-parameters} is te zien hoe dit kan worden toegepast. Hier is de mogelijkheid om parameters door te geven op basis van een keyword en kunnen de parameters worden doorgegeven in elke willekeurige volgorde. Deze mogelijkheid is niet gegeven in GoLang. 

\begin{listing}[H]
	\begin{minted}{python}
sumer(length = my_length,
	list = my_array,
	sum = my_sum)
	\end{minted}
	\caption{Keyword parameters}
	\label{code:keyword-parameters}
\end{listing}


Er zijn twee verschillende categorie\"{e}n van subprograms; \emph{procedures} en \emph{functions}. Functions retourneren waardes en procedures niet. In GoLang zijn procedures niet opgenomen omdat functions de mogelijkheid hebben om geen waardes te retourneren. 

\section{Parameter-Passing Methods}
\emph{Parameter-passing methods} zijn de manieren waarop parameters worden doorgegeven van en/of naar aangeroepen subprogramma's. Er zijn drie modellen, genaamd \emph{in mode}, \emph{out mode} en \emph{inout mode} die op verschillende manieren gebruikt kunnen worden.

\begin{description}
	\item [in mode] betekent dat de parameters worden doorgegeven en gebruikt worden voor hun data maar niet worden aangepast.
	\item [out mode] betekent dat een nieuwe variable wordt gemaakt en terug gegeven door het subprogramma als resultaat. 
	\item [inout mode] betekent dat een parameter's waarde wordt gebruikt. 
\end{description}

Er zijn vijf verschillende toepassingen van deze methoden; \emph{pass by value}, \emph{pass by result}, \emph{pass by value-result}, \emph{pass by reference} en \emph{pass by name}. 

\paragraph*{pass by value} Als een parameter op deze manier wordt doorgegeven, dan wordt de waarde ervan gebruikt om een formal parameter te initialiseren die als lokaal parameter wordt gebruikt in de subprogram. Dit is een vorm van \emph{in-mode semantics}. Een gangbare methode om pass by value te realiseren is door de waarde van de parameter te kopi\"{e}ren. In het code voorbeeld hieronder is te zien dat het geheugen adres van de parameter in de subprogram anders is dan die van de variable.

\begin{listing}[H]
	\begin{minted}{go}
package main

import (
	"fmt"
)

func passByValue(x int)  {
  fmt.Println(&x)
}

func main() {
  value := 15

  fmt.Println(&value)
  passByValue(value)

}

// Output:
// 0x416020
// 0x416040

	\end{minted}
	\label{code:pass-by-value}
	\caption{Pass by value}
\end{listing}

\paragraph*{Pass by result} Dit is een \emph{out mode} model. Dit betekent dat een subprogram een nieuwe aanmaakt die vervolgens terugkeert naar de aanroeper die moet worden gebruikt als de waarde voor een niet-ge\"{i}nitaliseerde variabele. De onderstaande code maakt twee lege variabelen, x en y, en gebruikt vervolgens een subprogramma-resultaat om ze waarden te geven:

\begin{listing}[H]
	\begin{minted}{go}
package main

import "fmt"

func split() (x, y int) {
	x = 5
	y = 15
	return
}

func main() {
	fmt.Println(split())
}

// Output:
// 5 15

	\end{minted}
	\caption{Pass by result}
	\label{code:pass-by-result}
\end{listing}

\paragraph*{Pass by reference} Dit is een andere implementatie van \emph{inout-mode} model. Anders dan bij \emph{pass by result-value} worden de waardes niet gekopieerd maar wordt het pad naar de waarde toe als parameter doorgegeven. In veel gevallen is dit het geheugen adres waar de waarde staat opgeslagen. Hieronder is een voorbeeld van hoe pointers werken. Hier is te zien dat de waarde van x wordt aangepast naar 0 en de waarde van y niet. 

\begin{listing}[H]
	\begin{minted}{go}
package main

import (
	"fmt"
)

func withPointer(pointer *int) {
  *pointer = 0
}

func withoutPointer(x int) {
  x = 0
}

func main() {
  x := 5
  withPointer(&x)
  fmt.Println(x) 
  
  y := 10
  withoutPointer(y)
  fmt.Println(y)
}

// Output:
// 0 
// 10
	\end{minted}
	\caption{Pass by reference}
	\label{code:pass-by-reference}
\end{listing}


\paragraph*{Pass by name} Dit is een vorm van \emph{inout-mode} dat niet overeenkomt met een enkel implementatie model. Wanneer parameters met pass by name worden doorgegeven, wordt de actual parameter feitelijk vervangen door de overeenkomstige formal parameter in het subprogramma. Dit is anders dan andere methoden waarbij formal parameters gebonden aan actual waarden of adressen op het moment van de oproep van het subprogramma.


\paragraph*{Variadic function} Anders dan bij sommige programmeertalen is het bij GoLang mogelijk om een variable aantal parameters te accepteren. Dit soort functies worden \emph{variadic functions} genoemd. Hieronder is een voorbeeld ervan.

\begin{listing}[H]
	\begin{minted}{go}
func sum(nums ...int) {
    fmt.Print(nums, " ")
    total := 0
    for _, num := range nums {
        total += num
    }
    fmt.Println(total)
}

func main() {
    sum(1, 2)
    sum(1, 2, 3)

    nums := []int{1, 2, 3, 4}
    sum(nums...)
}

// Output:
// [1 2] 3
// [1 2 3] 6
// [1 2 3 4] 10

	\end{minted}
	\caption{Variadic function}
	\label{code:variadic-function}
\end{listing}

In dit voorbeeld is te zien dat het mogelijk is om een variabel aantal parameters aan een functie mee te geven. De argumenten zijn dan beschikbaar als \emph{slice}. Je kan ook een slice als parameter mee geven. 

\section{Parameters That Are Subprograms}
Sommige subprograms kunnen worden gebruikt om variabelen te maken die worden gebruikt als input bij andere subprograms. In plaats van een variabele op te geven en deze vervolgens als parameter door te geven, kan een subprogramma direct worden doorgegeven. Mogelijke problemen die zich voordoen met (geneste) subprogramma's zijn type checking en referencing environments. 

Om het probleem met de referencing environments op te lossen, mag een geneste functie geen toegang krijgen tot de externe scope tenzij gebruik wordt gemaakt van sluitingen. Meestal zijn er drie mogelijke keuzes voor omgevingsomgevingen;
\begin{description}
	\item [Shallow binding] De omgeving waar de sub program is geconstrueerd word meegenomen.
	\item [Deep binding] De omgeving waar het sub program zich bevindt.
	\item [Ad hoc binding] De omgeving van diegene die de functie aanroept.
\end{description}

In GoLang zijn nested functions niet mogelijk. Het is wel mogelijk om een functie mee te geven als parameter of een anonieme functie in een variable stoppen. Er is geen officieel argument te vinden waarom deze functionaliteit niet is ge\"{i}mplementeerd maar waarschijnlijk om de complexiteit die het de compiler geeft niet opweegt tegen de baten die nested functions oplevert.

\section{Overloaded Subprograms}
\emph{Overloaded subprograms} zijn sub programs waarbij de naam hetzelfde is en alleen de formal parameters van de interface verschillen. Hierbij gaat het specifiek om subprograms die in dezelfde omgeving zitten. GoLang ondersteunt geen overloading. Hieronder is een voorbeeld te zien van overloading in Java. Hierbij zijn er twee methoden \emph{add} die allebei een andere aantal parameters hebben.

\begin{listing}[H]
	\begin{minted}{java}
class Example {  
	static int add(int a,int b) {
		return a+b;
	}  
	static int add(int a,int b,int c) {
		return a+b+c;
	}  
}  
	\end{minted}
	\caption{Overloading}
	\label{code:overloading}
\end{listing}

\section{Generic Subprograms}
Om software eenvoudig te hergebruiken zijn er \emph{generic subprograms} bedacht. Een \emph{polymorphic subprogram} gebruikt generieke parameters om het type van de parameters te beschrijven. Dit is niet beschikbaar in GoLang. In code voorbeeld \ref{code:generic-subprogram} is te zien hoe een generic subprogram er uit zien in Java. 

\begin{listing}[H]
	\begin{minted}{java}
public class GenericMethodTest {
   // generic method printArray
   public static < E > void printArray( E[] inputArray ) {
      // Display array elements
      for(E element : inputArray) {
         System.out.printf("%s ", element);
      }
      System.out.println();
   }

   public static void main(String args[]) {
      // Create arrays of Integer, Double and Character
      Integer[] intArray = { 1, 2, 3, 4, 5 };
      Double[] doubleArray = { 1.1, 2.2, 3.3, 4.4 };
      Character[] charArray = { 'H', 'E', 'L', 'L', 'O' };

      printArray(intArray);   

      printArray(doubleArray); 

      printArray(charArray);   
   }
}
	\end{minted}
	\label{code:generic-subprogram}
	\caption{Generic subprogram}
\end{listing}

\section{Design Issues for Functions}
Bij het implementeren van functies moet er rekening worden gehouden of het binnen de principes valt van de programmeertaal. Zo zijn er drie specifieke vraagstukken waarbij rekening mee gehouden moet worden: 

\begin{itemize}
	 \item Zijn side-effects toegestaan?
	 \item Wat zijn de type waardes die geretourneerd worden?
	 \item Hoeveel waardes mogen er geretourneerd worden?
\end{itemize}

\section{User-Defined Overloaded Operators}
Een \emph{user-defined overloaded operator} betekent dat de programmeur de bestaande operator mag uitbreiden met functionaliteiten. Dit is niet mogelijk in GoLang. In C++ is dit wel mogelijk zoals hieronder in een voorbeeld te zien is van het overloaden van de. \emph{+} operator.

\begin{listing}[H]
	\begin{minted}{c++}
Complex operator+(Complex &second) {
	return Complex(real + second.real, imag + second.imag);
}		
	\end{minted}
	\caption{Overloading + operator}
	\label{code:user-defined-overloading-operator}
\end{listing}

\section{Block}
Een \emph{block} is een gegroepeerde set aan een of meer uitvoerbare statements en worden bij GoLang gebruikt om onder andere de scope en de levensduur van de variabel te bepalen. Het begin van een block wordt aangeduid met een open accolade en het einde van een block gesloten accolade. 

\section{Go routines}
Een van de unieke eigenschappen dat Go zo krachtig maakt zijn de speciale subprograms \emph{Go routines}. Go routines zijn functies die \emph{concurrent} kunnen worden uitgevoerd met andere functies of methoden. Dit is een vorm van een licht gewicht thread. Hieronder is een voorbeeld te zien van een Go routine. In hoofdstuk \ref{chap:concurrency} wordt er verder uitgelegd wat \emph{concurrency} inhoud.

\begin{listing}[H]
	\begin{minted}{go}
package main

import (
	"fmt"
	"time"
)

func say(s string) {
	for i := 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		fmt.Println(s)
	}
}

func main() {
	go say("world")
	say("hello")
}

// Output
world
hello
hello
world
world
hello
hello
world
world
hello
	\end{minted}
	\caption{Go routine}
	\label{code:go-routine}
\end{listing}

