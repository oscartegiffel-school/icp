\chapter{Concurrency}
\label{chap:concurrency}
\emph{Concurrency} is de uitvoering van een sequentie aan statements die op hetzelfde moment plaatsvinden. Dit kan plaatsvinden op vier verschillende niveaus; 

\begin{itemize}
	\item Op \emph{instruction niveau} waarbij er twee of meer machine instructies parallel worden uitgevoerd.
	\item Op \emph{statement niveau} waarbij twee of meer high-level statements parallel worden uitgevoerd.
	\item Op \emph{unit niveau} waarbij twee of meer subprograms parallel worden uitgevoerd.
	\item Op \emph{program niveau} waarbij er twee of meer programma's parallel worden uitgevoerd. 
\end{itemize}

\section{Subprogram-Level Concurrency Semaphores}
Een \emph{task} (ook wel \emph{process} genoemd) is een deel van een programma en is vergelijk met een subprogram. Deze task kan parallel worden uitgevoerd met andere units van hetzelfde programma. In sommige programmeertalen kunnen methodes als taken worden uitgevoerd. Deze methoden worden dan uitgevoerd als objecten. Deze worden \emph{threads} genoemd. 

Er zijn drie verschillen tussen een tasks en een subprogram. Ten eerste,     een task impliciet gestart kan worden terwijl een subprogram expliciet moet worden aangeroepen. Ten tweede, wanneer een programma een taak aanroept hoeft deze in sommige gevallen niet te wachten tot de taak de uitvoering voltooit voordat hij verder gaat met zijn eigen taak. Ten derde, wanneer de uitvoering is voltooid kan de besturing al dan niet terugkeren naar de unit die de uitvoering is gestart.

Tasks kunnen in twee categorie\"{e}n vallen; \emph{lightweight} en \emph{heavyweight}. Een lightweight process wordt uitgevoerd in gedeeld geheugen en een heavyweight krijgt zijn eigen geheugen. 

Tasks kunnen communiceren met andere tasks door gedeelde niet lokale variabelen. Dit kan nodig zijn als processen met dezelfde dataset werken en de ze moeten samenwerken. 

Er kunnen honderden processen draaien en het is niet altijd gewenst om deze asynchroon uit te voeren. Processen kunnen worden beheert door \emph{synchronization}. Synchronization is en mechanisme dat beheert in welke volgorde taken worden uitgevoerd. Er zijn twee soorten synchronization; \emph{cooperation synchronization} en \emph{competition synchronization}. Het voormalige is vereist als twee taken afhankelijk van elkaar zijn. De laatste is vereist als twee taken aanspraak moeten doen op een bron die maar \'{e}\'{e}n taak per keer kan dienen. Deze methoden van synchronization brengen twee problemen met zich mee; \emph{producer-consumer problem} en de \emph{race condition}. 

\section{Semaphores}
Een \emph{semaphore} is een mechanisme dat kan worden gebruikt om tasks te synchroniseren. Het \emph{producer-consumer problem} kan een semaphore gebruiken om synchronisatie te krijgen tussen de producer en de consumer door twee semaphores te krijgen bijvoorbeeld \emph{empty} en \emph{full} om het aantal vrije en volle slots aan te duiden van de data structuur. In GoLang is er geen primitieve manier van semaphores maar hetzelfde resultaat kan bereikt worden met buffered channels. 

Om de toegang tot de data structuur te limiteren kunnen er \emph{guards} worden geplaatst om de code die toegang vraagt tot de structuur. Een guard zorgt ervoor dat de code in de guard maar \'{e}\'{e}n taak een stuk gedeelde data structuur toegang geeft.

Een semaphore is een datastructuur dat bestaat uit een integer en een queue dat \emph{task descriptors} opslaat. Een task descriptor is een data structuur dat alle relevante data van die task opslaat. 

\begin{listing}[H]
	\begin{minted}{go}
type empty {}
type semaphore chan empty

// A semaphore can be created through. N is the number of wanted resources
sem = make(semaphore, N)

// acquire n resources
func (s semaphore) P(n int) {
    e := empty{}
    for i := 0; i < n; i++ {
        s <- e
    }
}

// release n resources
func (s semaphore) V(n int) { 
    for i := 0; i < n; i++ {
        <-s
    }
}		
	\end{minted}
	\caption{Semaphore}
	\label{code:semaphore}
\end{listing}

\section{Monitors}
Een \emph{monitor} is een abstract data type dat toegang limiteert. Het gebruiken van een semaphore geeft synchronization aan de programmeur maar deze is nog steeds zelf verantwoordelijk exclusieve toegangen. Een monitor kan gebruikt worden om de verantwoordelijk over te dragen aan de compiler. Een monitor is een lock op een subprogram of data type zetten zodat alleen het huidige subprogram er toegang toe heeft. Hierdoor wordt het synchroon uitvoeren van een programma geforceerd. 

\section{Message Passing}
Een manier om concurrency veilig te doen is door \emph{message passing}. Message passing vereist een \emph{sender}, \emph{receiver} en een channel om de message doorheen te sturen. De makers van GoLang hebben dit standaard meegeleverd.  

De makers van GoLang wilde een taal maken die betere support had voor concurrency dan op dat moment het meest gangbare was. Daarom zitten er ingebouwde features om dit voor elkaar te krijgen in GoLang. In het vorige hoofdstuk was er al een voorproefje gegeven en hier wordt er verder op in gegaan. 

GoLang bevat \emph{goroutines}. Dit is een lichte vorm van threads. Hieronder is er een voorbeeld van gegeven.

\begin{listing}[H]
	\begin{minted}{go}
	package main

import (
  "fmt"
)

func main() {
  fmt.Println("This will happen first")

  go func() {
    fmt.Println("This will happen at some unknown time")
  }()

  fmt.Println("This will either happen second or third")

  fmt.Scanln()
  fmt.Println("done")
}
	\end{minted}
	\caption{Concurrency - Goroutines}
	\label{code:concurrency-goroutines}
\end{listing}

Met het keyword \emph{go} voor een functie wordt er aangegeven dat het een goroutine is. Het probleem hierbij is dat je niet weet wanneer de goroutine wordt uitgevoerd. In het voorbeeld hieronder is te zien wat dat voor problemen kan veroorzaken.

\begin{listing}[H]
	\begin{minted}{go}
package main

import (
  "fmt"
)

func main() {
  a := 1
  b := 2

  go func() {
    b = a * b
  }()

  a = b * b

  fmt.Println("Hit Enter when you want to see the answer")
  fmt.Scanln()

  fmt.Printf("a = %d, b = %d\n", a, b)
}

// Output 
// Hit Enter when you want to see the answer
// 
// a = 4, b = 8
	\end{minted}
	\caption{Concurrency - thread problem}
	\label{code:concurrency-thread-problem}
\end{listing}

Hierbij is te zien dat b gelijk is aan 8 terwijl je zou verwachten dat b gelijk is aan 2. De functie op regel 12 wordt op een later moment uitgevoerd door de compiler. Het kan dus zijn dat door threads te gebruiken je onverwacht gedrag krijgt. 

GoLang heeft hier een oplossing voor; \emph{channels}. Channels geven de mogelijkheid voor veilige communicatie tussen verschillende threads. Hieronder is een voorbeeld te zien van een channel gebruiken met threads.

\begin{listing}[H]
	\begin{minted}{go}
package main

import (
  "fmt"
)

func main() {
  a := 1
  b := 2

  operationDone := make(chan bool)
  go func() {
    b = a * b

    operationDone <- true
  }()

  <-operationDone

  a = b * b

  fmt.Println("Hit Enter when you want to see the answer")
  fmt.Scanln()

  fmt.Printf("a = %d, b = %d\n", a, b)
}		
	\end{minted}
	\caption{Concurrency - channels}
	\label{code:concurrency-channels}
\end{listing}

Op regel 11 in dit codevoorbeeld wordt er een channel gemaakt die booleans accepteert en teruggeeft. Op regel 15 wordt er de boolean \emph{true} aan de channel toegevoegd als de asynchrone functie klaar is. Op regel 18 wordt er gezegd dat de \emph{main thread} moet wachten tot dat er een bericht aan de channel wordt toegevoegd. 

\section{Statement-Level Concurrency}
Processen tegelijkertijd uitvoeren is de gebruikelijke manier om gelijktijdige code te schrijven. In sommige talen is het mogelijk om aan de compiler door te geven dat specifieke code parallel kan worden uitgevoerd in bijvoorbeeld een for-loop. Als de CPU multithreading ondersteund dan kan de compiler deze specifieke code parallel uitvoeren. 

GoLang heeft niet deze feature, maar C\# wel. Hieronder is een voorbeeld in C\# van een \emph{Parallel.For} in C\# van de offici\"{e}le Windows documentatie.

\begin{listing}[H]
	\begin{minted}{csharp}
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

public class Example
{
   public static void Main()
   {
      long totalSize = 0;

      String[] args = Environment.GetCommandLineArgs();
      if (args.Length == 1) {
         Console.WriteLine("There are no command line arguments.");
         return;
      }
      if (! Directory.Exists(args[1])) {
         Console.WriteLine("The directory does not exist.");
         return;
      }

      String[] files = Directory.GetFiles(args[1]);
      Parallel.For(0, files.Length,
                   index => { FileInfo fi = new FileInfo(files[index]);
                              long size = fi.Length;
                              Interlocked.Add(ref totalSize, size);
                   } );
      Console.WriteLine("Directory '{0}':", args[1]);
      Console.WriteLine("{0:N0} files, {1:N0} bytes", files.Length, totalSize);
   }
}
// The example displaysoutput like the following:
//       Directory 'c:\windows\':
//       32 files, 6,587,222 bytes
	\end{minted}
	\caption{Parallel.For}
	\label{code:parallel-for}
\end{listing}


\section{Locking}
\label{section:locking}
Een \emph{lock} of \emph{mutex} is een synchronisatie mechanisme om toegang tot een resource te limiteren. Het wordt gebruikt om concurrency te verkrijgen door wederzijdse uitsluiting. 

Er zijn verschillende soorten locks maar in het algemeen zijn er \emph{advisory locks}. Een thread verkrijgt eerst de lock voordat er gebruikt gemaakt kan worden van een resource. Sommige systemen implementeren ook een \emph{mandatory lock}. Bij toegang vragen tot een resource wordt er eerst gekeken of er een lock is. Als dat niet het geval is geeft het systeem een exceptie. 


De mogelijkheid om een resource te locken zit standaar in GoLang. Hieronder is een voorbeeld er van te zien. 

\begin{listing}[H]
	\begin{minted}{go}
package main

import (
	"fmt"
	"sync"
	"time"
)


type SafeCounter struct {
	v   map[string]int
	mux sync.Mutex
}

// Inc increments the counter for the given key.
func (c *SafeCounter) Inc(key string) {
	c.mux.Lock()
	// Lock so only one goroutine at a time can access the map c.v.
	c.v[key]++
	c.mux.Unlock()
}

// Value returns the current value of the counter for the given key.
func (c *SafeCounter) Value(key string) int {
	c.mux.Lock()
	// Lock so only one goroutine at a time can access the map c.v.
	defer c.mux.Unlock()
	return c.v[key]
}	
	\end{minted}
	\caption{Locking}
	\label{code:locking}
\end{listing}


\section{Deadlock}
In subsectie \ref{section:locking} is er uitgelegd over \emph{locking} en hoe het resources vastzet voor een bepaalde thread zodat alleen die thread beschikbaarheid heeft over de resource. Het gebruiken van locking brengt veel voordelen met zich mee maar ook nadelen. Het zorgt voor extra \emph{overhead} en het is dan mogelijk om \emph{deadlocks} te krijgen.

Deadlock is wanneer ten minste twee taken wacht op een vergrendeling die de andere taak bevat. Tenzij iets wordt gedaan, zullen de twee taken voor altijd op elkaar wachten. 



